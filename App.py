
from flask import Flask,render_template,request,redirect,url_for,flash

from flask_mysqldb import MySQL

app = Flask(__name__)
#mariadb+pymysql://root:password@127.0.0.1:3306/words

#Mysql Connection
app.config['MYSQL_HOST']='127.0.0.1'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='password'
app.config['MYSQL_DB']='wordsflask'
mysql = MySQL(app)

#settings
app.secret_key = 'mysecretkey'


@app.route('/')
def Index():
    curl = mysql.connection.cursor()
    curl.execute('SELECT * FROM words')
    data = curl.fetchall()
    print(data)
    return render_template('index.html',words=data)

@app.route('/add_word',methods=['POST'])
def add_word():
    if request.method == 'POST':
        word = request.form['word']
        description = request.form['description']
    
        
        curl = mysql.connection.cursor()
        curl.execute('INSERT INTO words (word,description) VALUES (%s,%s)',(word,description))
        mysql.connection.commit()
        
    flash('Palabra Agregada correctamente')    
    return redirect(url_for('Index'))

@app.route('/edit/<id>')
def edit_word(id):
    curl = mysql.connection.cursor()
    curl.execute('SELECT * FROM words WHERE id = %s',(id))
    data = curl.fetchall()

    return render_template('edit.html',word = data[0])

@app.route('/update/<id>', methods = ['POST'])
def update_word(id):
    if (request.method =='POST'):
        word = request.form['word']
        description = request.form['description']
        curl = mysql.connection.cursor()
        curl.execute('UPDATE words SET word = %s,description = %s WHERE id = %s ',(word,description,id))
        mysql.connection.commit()
        flash('Palabra Actualizada')
    return redirect(url_for('Index'))

@app.route('/delete/<string:id>')
def delete_word(id):
    curl = mysql.connection.cursor()
    curl.execute('DELETE FROM words WHERE id = {0}'.format(id))
    mysql.connection.commit()
    flash('Palabra removida correctamente')
    return redirect(url_for('Index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0')